package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.work.Data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class RegistroMascotas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_mascotas);
        System.out.println(recibirdatos(1));

        //Se muestra un mensaje para que el usuario sepa el tipo de mascota que seleccionó
        Toast.makeText(this,recibirdatos(2),Toast.LENGTH_SHORT).show();

        //Se configura la lista desplegable de acuerdo a la selección nombrada anteriormente
        Spinner spinner1 = findViewById(R.id.raza);
        String [] opciones=menu_desplegable(recibirdatos(2));
        ArrayAdapter <String> adapter= new ArrayAdapter<>(this,R.layout.spinner_item_calcular, opciones);
        spinner1.setAdapter(adapter);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new java.util.Date());
        String imageFileName = "PNG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".png",   /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents

    }
    private Uri uris;
    private static final int REQUEST_TAKE_PHOTO = 1;
    public void dispatchTakePictureIntent_BUTTON(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Toast.makeText(this,"Toque el gato para ver la foto, en algunos casos tendra que tomar la foto en horizontal",Toast.LENGTH_LONG).show();
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(this,"Error occurred while creating the File",Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                uris=photoURI;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    public void cuadrar_foto_BUTTON(View view){
        try{
            ImageView gato=findViewById(R.id.foto_mascota);
            gato.setImageURI(uris);
        }catch (Exception ex){
            Toast.makeText(this,"Por favor tome una foto",Toast.LENGTH_SHORT).show();
        }
    }

    public void registrarMascota_BUTTON(View view){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();

        //Se relaciona la parte visual con la logica
        Spinner spinner1 = findViewById(R.id.raza);
        TextView NombreTV=findViewById(R.id.nombre_mascota);
        TextView EdadTV=findViewById(R.id.edad_mascota);

        //Se declaran las variables donde se almacenara la información
        String nombre=NombreTV.getText().toString();
        String edad=EdadTV.getText().toString();
        String rasgo_distintivo=spinner1.getSelectedItem().toString();

        //Se declaran variables adicionales para verificar los datos y facilitar su inserción en una base de datos
        String id="";
        boolean registro_completo=false;

        //Se verifica que los campos esten completos y que la fecha sea valida
        if(!nombre.isEmpty()&&!edad.isEmpty()&& !rasgo_distintivo.equals("Especie")&&!rasgo_distintivo.equals("Caracteristicas")){
            if(Integer.parseInt(edad)<=100){
                registro_completo=true;
            }else{
                Toast.makeText(this,"Edad invalida",Toast.LENGTH_SHORT).show();
                EdadTV.setText("");
            }
        }else{
            Toast.makeText(this,"Por favor complete todos los datos",Toast.LENGTH_SHORT).show();
        }

        if(registro_completo){
            int i=1;
            while (true){
                //busqueda de mascotas registradas
                String it=Integer.toString(i);
                Cursor cursor=BaseDeDatos.rawQuery("select nombre, id_dueno from mascotas where id ="+it,null);
                //asignacion de id para nueva mascota
                if(!cursor.moveToFirst()){
                    cursor.close();
                    id=it;
                    System.out.println("Asignacion OK");
                    break;
                }else{
                    System.out.println("Busqueda OK");
                    i++;
                }
            }
        }

        String imagepath;
        try {
            imagepath=uris.toString();
        }catch (Exception ex){
            imagepath="Falta la foto";
        }
        if(registro_completo&&!imagepath.equals("Falta la foto")){
            ContentValues contentValues=new ContentValues();
            contentValues.put("id",id);
            contentValues.put("especie",recibirdatos(2));
            contentValues.put("nombre",nombre);
            contentValues.put("edad",edad);
            contentValues.put("rasgo_distintivo",rasgo_distintivo);
            contentValues.put("imagepath",imagepath);
            contentValues.put("id_dueno",recibirdatos(1));
            BaseDeDatos.insert("mascotas",null,contentValues);
            BaseDeDatos.close();
            System.out.println(id);
            System.out.println(recibirdatos(2));
            System.out.println(nombre);
            System.out.println(edad);
            System.out.println(rasgo_distintivo);
            System.out.println(imagepath);
            System.out.println(recibirdatos(1));
            @SuppressLint("RestrictedApi")
            Data data=new Data.Builder().put("titulo","Su mascota "+nombre+" ha sido registrada").
                    put("detalle","Ya puede empezar a programarle rutinas").build();
            WorkManagerNotification.guardarNotificacion(-1,data,"0");
            finish();
        }else if(registro_completo){
            Toast.makeText(this,"Por favor tome una foto",Toast.LENGTH_SHORT).show();
        }
    }

    private String recibirdatos(int i) {
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        if(i==1) {
            return extras.getString("id");
        }else{
            return extras.getString("Tipo Seleccionado");
        }
    }

    private String[] menu_desplegable(String tipo_seleccionado){
        switch (tipo_seleccionado){
            case "Aves Ornamentales":
                return new String[]{"Especie","Canario","Loro","Cacatua Ninfa","Periquito Australiano","Pinzones","Bengalí","Otra"};
            case "Perros":
                return new String[]{"Caracteristicas","Pequeño con pelo corto","Pequeño con pelo largo",
                        "Mediano con pelo corto","Mediano con pelo largo","Grande con pelo corto","Grande con pelo largo"};
            case "Peces de acuario":
                return new String[]{"Especie","Pez dorado (Goldfish)","Guppy","Betta","Ángel","Platys","Killis","Otra"};
            case "Roedores":
                return new String[]{"Especie","Hamster","Cobaya","Jerbo","Chinchilla","Otra"};
            case "Gatos":
                return new String[]{"Caracteristicas","Pelo corto","Pelo largo"};
            default:
                return new String[]{"Error","Trate nuevamente"};
        }
    }
}