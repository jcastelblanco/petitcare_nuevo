package com.petitcare;

public class User {
    private String id;
    private String nombre;
    private String correo;
    private String password;

    public User(String id, String correo, String password,String nombre) {
        this.id = id;
        this.correo = correo;
        this.password = password;
        this.nombre= nombre;
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public String getPassword() {
        return password;
    }
}