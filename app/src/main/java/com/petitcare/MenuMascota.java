package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuMascota extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_mascota);
        System.out.println(recibirdatos(1));

        TextView nombreT=findViewById(R.id.nombre);
        TextView especieT=findViewById(R.id.c_etiqueta);
        TextView edadT = findViewById(R.id.edad);
        TextView rasgo_distintivoT=findViewById(R.id.rasgo_distintivo);
        ImageView foto_mascota=findViewById(R.id.foto_mascota);

        GestorDeMascotaSeleccionada.menuMascotas(recibirdatos(2),especieT,nombreT,
                edadT,rasgo_distintivoT,foto_mascota);

    }

    private String recibirdatos(int i){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        if(i==1) {
            return extras.getString("id");
        }else{
            return extras.getString("id_mascota");
        }
    }

    public void gestionDeMascotas_BUTTON(View view){
        Intent intent=new Intent(this,GestionMascotas.class);
        intent.putExtra("id",recibirdatos(1));
        intent.putExtra("id_mascota",recibirdatos(2));
        startActivity(intent);
        finish();
    }

    public void rutinasYVacunas_BUTTON(View view){
        Intent intent=new Intent(this,RutinasYVacunas.class);
        intent.putExtra("id",recibirdatos(1));
        startActivity(intent);
    }

    public void recomendaciones_BUTTON(View view){
        Intent intent=new Intent(this,RecomendacionesSegunMascota.class);
        switch (GestorDeMascotaSeleccionada.getEspecie()){
            case "Aves Ornamentales":
                intent.putExtra("descripcion","R_AVE");
                break;
            case "Perros":
                intent.putExtra("descripcion","R_PERRO");
                break;
            case "Peces de acuario":
                intent.putExtra("descripcion","R_PEZ");
                break;
            case "Roedores":
                intent.putExtra("descripcion","R_ROEDOR");
                break;
            case "Gatos":
                intent.putExtra("descripcion","R_GATO");
                break;
            default:
                intent.putExtra("descripcion","R_GENERAL");
                break;
        }
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            Intent intent=new Intent(this,MisMascotas.class);
            intent.putExtra("id",recibirdatos(1));
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}