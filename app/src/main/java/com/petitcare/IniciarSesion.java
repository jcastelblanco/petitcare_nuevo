package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;



public class IniciarSesion extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);
    }

    public void iniciarSesion_BUTTON(View view){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();
        //conexion formularios con codigo
        TextView emailusuarioT=findViewById(R.id.email_usuario);
        TextView contrasenausuarioT=findViewById(R.id.contrasena_usuario);
        //variables para almacenar los datos de los formularios y verificar la sesion
        String email=emailusuarioT.getText().toString();
        String contrasena=contrasenausuarioT.getText().toString();
        String id="0";
        boolean sesioniniciada=false;
        //comprobacion de que esten llenos los espacios
        int icono = R.mipmap.aaaa_pecera;
        if(!email.isEmpty()&&!contrasena.isEmpty()&&!email.equals("EMPTY")){
            int i=1;
            while (true){
                //busqueda de usuarios registrados
                id=Integer.toString(i);
                Cursor cursor = BaseDeDatos.rawQuery
                        ("select icono,email,contrasena,nombres from usuarios where id= " + id, null);
                if(cursor.moveToFirst()){
                    String compemail=cursor.getString(1);
                    System.out.println("e-mail repetido ="+compemail);
                    String compcontrasena=cursor.getString(2);
                    System.out.println("contrasena repetido ="+compcontrasena);
                    String iconoString=cursor.getString(0);
                    String userName=cursor.getString(3);
                    System.out.println(userName);
                    //inicio de sesion si hay coincidencia
                    if(compemail.equals(email)){
                        BaseDeDatos.close();
                        cursor.close();
                        System.out.println("e-mail registrado OK");
                        //verificacion de contraseña
                        if(compcontrasena.equals(contrasena)) {
                            sesioniniciada=true;
                            icono=Integer.parseInt(iconoString);
                            System.out.println("contraseña registrada OK");

                        }else{
                            //mensaje si existe el e-mail pero la contraseña no coincide
                            Toast.makeText(this,"Contraseña incorrecta",Toast.LENGTH_SHORT).show();
                            System.out.println("Contraseña mal OK");
                        }
                        break;
                    }
                    System.out.println("Busqueda OK");
                }else{
                    //mensaje si el e-mail no es encontrado
                    Toast.makeText(this,"El usuario no existe",Toast.LENGTH_SHORT).show();
                    BaseDeDatos.close();
                    cursor.close();
                    System.out.println("No hay usuario OK");
                    break;
                }
                i++;
            }
        }else{
            //alerta si hay espacios vacios
            Toast.makeText(this,"Hay espacios vacios",Toast.LENGTH_SHORT).show();
        }

        if(sesioniniciada){
            //Cambio de pantalla pero conservando el id del usuario que inicio sesion
            Toast.makeText(this,"Sesión iniciada correctamente",Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(this,Menu.class);
            intent.putExtra("id",id);
            intent.putExtra("icono",icono);
            startActivity(intent);
            finish();
        }
    }
}
