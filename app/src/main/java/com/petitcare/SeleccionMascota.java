package com.petitcare;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class SeleccionMascota extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccion_mascota);
        System.out.println(recibirdatos());
    }

    //Metodo para detectar la mascota (en imagen) seleccionada
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void mascotaSeleccionada_BUTTON(View view){
        String description=view.getContentDescription().toString();
        System.out.println(description);
        System.out.println(recibirdatos());
        Intent intent=new Intent(this, RegistroMascotas.class);
        intent.putExtra("Tipo Seleccionado",description);
        intent.putExtra("id",recibirdatos());
        startActivity(intent);
        finish();
    }

    private String recibirdatos() {
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }
}
