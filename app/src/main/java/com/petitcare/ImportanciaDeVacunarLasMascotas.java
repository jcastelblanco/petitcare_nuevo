package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class ImportanciaDeVacunarLasMascotas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_importancia_de_vacunar_las_mascotas);
        System.out.println(recibirdatos());
    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }
}