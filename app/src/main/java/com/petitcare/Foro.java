package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Foro extends AppCompatActivity {

    private TextView tTitulo;
    private TextView tCuerpo;

    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foro);

        tTitulo = (TextView)findViewById(R.id.titulo_resultado);
        tCuerpo = (TextView)findViewById(R.id.texto_resultado);
        String titulo = getIntent().getStringExtra("titulo_texto");
        tTitulo.setText((titulo));
        String cuerpo = getIntent().getStringExtra("cuerpo_texto");
        tCuerpo.setText(cuerpo);

        System.out.println(titulo);
        System.out.println(cuerpo);
        String id = recibirdatos();
        System.out.println(id);
        Post post = new Post(id,titulo,cuerpo);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("Publicaciones").child(id).setValue(post);
    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }
}

