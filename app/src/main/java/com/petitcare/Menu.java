package com.petitcare;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class Menu extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        System.out.println(recibirdatos());
        ImageButton usuario=findViewById(R.id.usuario);
        usuario.setImageResource(recibiricono());
        confirmacion=0;
    }

    public void usuario_BUTTON(View view) {
        Intent intent = new Intent(this, GestionUsuario.class);
        intent.putExtra("id", recibirdatos());
        intent.putExtra("icono",recibiricono());
        startActivity(intent);
        finish();
    }

    public void misMascotas_BUTTON(View view) {
        Intent intent = new Intent(this, MisMascotas.class);
        intent.putExtra("id",recibirdatos());
        startActivity(intent);
    }

    public void rutinas_BUTTON(View view){
        Intent intent = new Intent(this, Rutinas.class);
        intent.putExtra("id",recibirdatos());
        intent.putExtra("Es una mascota","NO");
        startActivity(intent);
    }

    public void parteSocial_BUTTON(View view){
        //Toast.makeText(this,"Proximamente...",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Social.class);
        intent.putExtra("id",recibirdatos());
        startActivity(intent);
    }

    private String recibirdatos() {
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }

    private int recibiricono(){
        Bundle extras=getIntent().getExtras();
        assert extras!=null;
        return extras.getInt("icono",R.mipmap.aaaa_pecera);
    }

    private int confirmacion;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            if(confirmacion==0){
                Toast.makeText(this,"Al salir se cerrara su sesión",Toast.LENGTH_LONG).show();
                confirmacion++;
            }else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}