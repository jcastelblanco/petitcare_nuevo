package com.petitcare;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import java.util.concurrent.TimeUnit;

public class WorkManagerNotification extends Worker {

    @SuppressLint("StaticFieldLeak")
    private static Context myContext;

    public static void setMyContext(Context myContext) {
        WorkManagerNotification.myContext = myContext;
    }

    public static Context getMyContext() {
        return myContext;
    }

    public WorkManagerNotification(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    public static void guardarNotificacion(long tEspera, Data data, String tag){
        OneTimeWorkRequest notificacion=new OneTimeWorkRequest
                .Builder(WorkManagerNotification.class)
                .setInitialDelay(tEspera, TimeUnit.MILLISECONDS)
                .addTag(tag).setInputData(data).build();
        WorkManager.getInstance(myContext).enqueue(notificacion);
        System.out.println("Notificación creada");
    }

    public static void guardarNotificacionPeriodica(long tEspera,int periodicidad,Data data,String tag){
        OneTimeWorkRequest notificaciones=new OneTimeWorkRequest
                .Builder(WorkManagerNotification.class)
                .setInitialDelay(tEspera, TimeUnit.MILLISECONDS)
                .addTag(tag).setInputData(data).build();
        PeriodicWorkRequest notificacionP=new PeriodicWorkRequest
                .Builder(WorkManagerNotification.class,periodo(periodicidad),TimeUnit.MILLISECONDS)
                .setInitialDelay(tEspera,TimeUnit.MILLISECONDS)
                .addTag(tag).setInputData(data).build();
        WorkManager workManager=WorkManager.getInstance(myContext);
        workManager.beginWith(notificaciones).enqueue();
        workManager.enqueueUniquePeriodicWork(tag, ExistingPeriodicWorkPolicy.KEEP,notificacionP);
        System.out.println("Notificación periodica creada");
    }

    @NonNull
    @Override
    public Result doWork() {
        String titulo=getInputData().getString("titulo");
        String detalle=getInputData().getString("detalle");
        int id=getInputData().getInt("id",0);
        int icon=getInputData().getInt("icon",R.mipmap.heart_paw_red);
        notificacion(titulo,detalle,id,icon);
        return Result.success();
    }

    private final String CHANNEL_ID="CHANNEL";
    private void createNotificationChannel(){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            CharSequence name="Notification";
            NotificationChannel notificationChannel=new NotificationChannel(CHANNEL_ID,name, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableVibration(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            Context context=getApplicationContext();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void notificacion(String titulo, String detalle, int NOTIFICATION_ID, int icon){
        createNotificationChannel();

        NotificationCompat.Builder builder=new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID);
        builder.setSmallIcon(icon);
        builder.setContentTitle(titulo);
        builder.setContentText(detalle);
        builder.setColor(Color.BLUE);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setVibrate(new long[]{500,500,500,500,500,500,1000});
        builder.setDefaults(Notification.DEFAULT_SOUND);
        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
        TaskStackBuilder taskStackBuilder=TaskStackBuilder.create(getApplicationContext());
        taskStackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent=taskStackBuilder.getPendingIntent(1,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(getApplicationContext());
        notificationManagerCompat.notify(NOTIFICATION_ID,builder.build());
    }

    private static long periodo(int tiempo){
        final long day_in_milis=(1000*60*60*24);
        switch (tiempo){
            case 1:
                //12h
                return (day_in_milis/2);
            case 2:
                //1d
                return day_in_milis;
            case 3:
                //7d
                return (day_in_milis*7);
            case 4:
                //30d
                return (day_in_milis*30);
            default:
                //1h
                return (day_in_milis/24);
        }
    }
}
