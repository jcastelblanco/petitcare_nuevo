package com.petitcare;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class AdminSQLiteOpenHelper extends SQLiteOpenHelper{

    AdminSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase BaseDeDatos) {

        BaseDeDatos.execSQL("create table usuarios " +
                "(id int primary key, icono int not null,nombres text not null, apellidos text not null, " +
                "fecha_de_nacimiento text not null, email text not null, contrasena text not null)");

        BaseDeDatos.execSQL("create table mascotas (id int primary key,especie text not null, " +
                "nombre text not null, edad int not null, rasgo_distintivo text not null, " +
                "imagepath text not null, id_dueno int not null)");

        BaseDeDatos.execSQL("create table carnet_de_vacunacion(id int primary key,nombre text not null" +
                ",fecha Date not null, veterinaria text not null, inyectadopor text not null," +
                "id_mascota int not null)");

        BaseDeDatos.execSQL("create table notificaciones(id int primary key,titulo text not null," +
                "texto text not null, id_mascota int not null, fecha_programada Date not null," +
                "concepto text not null, id_usuario text not null)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}