package com.petitcare;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import androidx.work.Data;
import androidx.work.WorkManager;

public class EmergentDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        final View view= requireActivity().getLayoutInflater().inflate(R.layout.alert_dialog,null);
        final TextView codigo=view.findViewById(R.id.codigo);

        builder.setView(view)
                .setTitle("Eliminar Rutina").setNeutralButton("Eliminar", new DialogInterface.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String tag=codigo.getText().toString();
                if(!tag.isEmpty()){
                    System.out.println("eliminando");
                    WorkManager.getInstance(WorkManagerNotification.getMyContext()).cancelAllWorkByTag(tag);
                        @SuppressLint("RestrictedApi")
                    Data data=new Data.Builder().put("titulo","Rutina Eliminada (cod:"+tag+")").
                            put("detalle","No se mostraran más notificaciones").build();
                    WorkManagerNotification.guardarNotificacion(-1,data,"0");
                    System.out.println("ELIMINADO");
                    }
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        return builder.create();
    }
}
