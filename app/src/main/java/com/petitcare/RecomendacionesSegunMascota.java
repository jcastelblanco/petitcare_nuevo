package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

public class RecomendacionesSegunMascota extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomendaciones_segun_mascota);
        WebView webView=findViewById(R.id.recomendaciones_web);
        switch(recibirdatos()){
            case "R_ROEDOR":
                Toast.makeText(this,"CARGANDO . . .",Toast.LENGTH_LONG).show();
                webView.loadUrl("https://roedoresdomesticos.com/");
                break;
            case "R_AVE":
                Toast.makeText(this,"CARGANDO . . .",Toast.LENGTH_SHORT).show();
                webView.loadUrl("https://www.expertoanimal.com/las-mejores-aves-para-tener-en-casa-23466.html");
                break;
            case "R_PERRO":
                Toast.makeText(this,"CARGANDO . . .",Toast.LENGTH_SHORT).show();
                webView.loadUrl("https://www.expertoanimal.com/cuidados-de-perros-cachorros-7442.html");
                break;
            case "R_GATO":
                Toast.makeText(this,"CARGANDO . . .",Toast.LENGTH_SHORT).show();
                webView.loadUrl("https://www.expertoanimal.com/cuidados-de-gatos-cachorros-21217.html");
                break;
            case "R_PEZ":
                Toast.makeText(this,"CARGANDO . . .",Toast.LENGTH_SHORT).show();
                webView.loadUrl("https://www.webanimales.com/consejos/peces/general/los-peces-como-mascotas");
                break;
            default:
                Toast.makeText(this,"CARGANDO . . .",Toast.LENGTH_SHORT).show();
                webView.loadUrl("https://www.expertoanimal.com/como-elegir-la-mascota-adecuada-22361.html");
                break;
        }
    }

    private String recibirdatos(){
        Bundle extras=getIntent().getExtras();
        assert extras!=null;
        return extras.getString("descripcion");
    }
}