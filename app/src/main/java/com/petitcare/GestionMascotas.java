package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.work.Data;
import androidx.work.WorkManager;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class GestionMascotas extends AppCompatActivity {

    private int i;
    private String email;
    private String contrasena;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_mascotas);
        i=1;
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();
        Cursor cursor=BaseDeDatos.rawQuery
                ("select email, contrasena from usuarios where id="+recibirdatos(1),null);
        if(cursor.moveToFirst()){
            email=cursor.getString(0);
            contrasena=cursor.getString(1);
        }
        cursor.close();
        BaseDeDatos.close();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new java.util.Date());
        String imageFileName = "PNG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        // Save a file: path for use with ACTION_VIEW intents
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".png",   /* suffix */
                storageDir      /* directory */
        );
    }

    private Uri uris;
    private static final int REQUEST_TAKE_PHOTO = 1;
    public void dispatchTakePictureIntent_BUTTON(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Toast.makeText(this,"Toque el gato para ver la foto, en algunos casos tendra que tomar la foto en horizontal",Toast.LENGTH_LONG).show();
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(this,"Error occurred while creating the File",Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                uris=photoURI;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    public void cuadrar_foto_BUTTON(View view){
        try{
            ImageView gato=findViewById(R.id.foto_nueva);
            gato.setImageURI(uris);
        }catch (Exception ex){
            Toast.makeText(this,"Por favor tome una foto",Toast.LENGTH_SHORT).show();
        }
    }

    public void guardarCambios_BUTTON(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        TextView confirmaremailT = findViewById(R.id.confirmaremail2);
        TextView confirmarcontrasenaT = findViewById(R.id.confirmarcontrasena2);
        String confirmaremail=confirmaremailT.getText().toString();
        String confirmarcontrasena=confirmarcontrasenaT.getText().toString();
        if(!confirmaremail.isEmpty()&&!confirmarcontrasena.isEmpty()){
            if(confirmarcontrasena.equals(contrasena)&&confirmaremail.equals(email)) {
                TextView edadT=findViewById(R.id.edad_mascota2);

                String edad=edadT.getText().toString();
                String imagepath;
                try {
                    imagepath=uris.toString();
                }catch (Exception ex){
                    imagepath="ALGO";
                }
                int cambioscompletos=0;
                if(!edad.isEmpty()&&!imagepath.equals("ALGO")){
                    ContentValues contentValues=new ContentValues();
                    contentValues.put("edad",edad);
                    contentValues.put("imagepath",imagepath);
                    cambioscompletos=BaseDeDatos.update("mascotas",contentValues,"id="+GestorDeMascotaSeleccionada.getId(),null);
                }else if(edad.isEmpty()&&!imagepath.equals("ALGO")){
                    ContentValues contentValues=new ContentValues();
                    contentValues.put("imagepath",imagepath);
                    cambioscompletos=BaseDeDatos.update("mascotas",contentValues,"id="+GestorDeMascotaSeleccionada.getId(),null);
                }else if(!edad.isEmpty()){
                    ContentValues contentValues=new ContentValues();
                    contentValues.put("edad",edad);
                    cambioscompletos=BaseDeDatos.update("mascotas",contentValues,"id="+GestorDeMascotaSeleccionada.getId(),null);
                } else {
                    Toast.makeText(this,"Modifique al menos un valor para poder guardar los cambios",Toast.LENGTH_SHORT).show();
                }
                if(cambioscompletos==1){
                    Toast.makeText(this,"Cambios realizados",Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(this,MenuMascota.class);
                    intent.putExtra("id",recibirdatos(1));
                    intent.putExtra("id_mascota",GestorDeMascotaSeleccionada.getId());
                    startActivity(intent);
                    finish();
                }
            }else{
                Toast.makeText(this,"Verificación de identidad invalida",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this,"Por favor llene la verificación de identidad",Toast.LENGTH_SHORT).show();
        }
    }

    public void eliminar_mascota_BUTTON(View view) {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        TextView confirmaremailT = findViewById(R.id.confirmaremail2);
        TextView confirmarcontrasenaT = findViewById(R.id.confirmarcontrasena2);
        String confirmaremail=confirmaremailT.getText().toString();
        String confirmarcontrasena=confirmarcontrasenaT.getText().toString();
        if(!confirmaremail.isEmpty()&&!confirmarcontrasena.isEmpty()){
            if(confirmarcontrasena.equals(contrasena)&&confirmaremail.equals(email)) {
                if (i == 1) {
                    Toast.makeText(this, "Oprima otra vez para eliminar a "+GestorDeMascotaSeleccionada.getNombre()+" de la aplicación", Toast.LENGTH_LONG).show();
                    i++;
                } else if (i == 2) {
                    int eliminado = BaseDeDatos.delete("mascotas", "id=" + GestorDeMascotaSeleccionada.getId(), null);
                    if (eliminado == 1) {
                        i=1;
                        int k=-10;
                        while (true) {
                            String id=Integer.toString(i);
                            Cursor cursor = BaseDeDatos.
                                    rawQuery("select id_mascota from notificaciones where id=" + id, null);
                            if(cursor.moveToFirst()){
                                String id_mascota=cursor.getString(0);
                                if(id_mascota.equals(GestorDeMascotaSeleccionada.getId())){
                                    WorkManager.getInstance(WorkManagerNotification.getMyContext()).cancelAllWorkByTag(id);
                                    BaseDeDatos.delete("notificaciones","id="+id,null);
                                }
                            }else if (k > 35) {
                                cursor.close();
                                break;
                            }
                            i++;
                            k++;
                        }
                        i=1;
                        k=-10;
                        while (true) {
                            String id=Integer.toString(i);
                            Cursor cursor = BaseDeDatos.
                                    rawQuery("select id_mascota from carnet_de_vacunacion where id=" + id, null);
                            if(cursor.moveToFirst()){
                                String id_mascota=cursor.getString(0);
                                if(id_mascota.equals(GestorDeMascotaSeleccionada.getId())){
                                    BaseDeDatos.delete("carnet_de_vacunacion","id="+id,null);
                                }
                            }else if (k > 35) {
                                cursor.close();
                                break;
                            }
                            i++;
                            k++;
                        }
                        @SuppressLint("RestrictedApi")
                        Data data=new Data.Builder().put("titulo","Mascota Eliminada").
                                put("detalle","También se borraron rutinas y vacunas asociadas").build();
                        WorkManagerNotification.guardarNotificacion(-1,data,"0");
                        BaseDeDatos.close();
                        finish();
                    }
                }
            }else{
                Toast.makeText(this,"Verificación de identidad invalida",Toast.LENGTH_SHORT).show();
                i=1;
            }
        }else{
            Toast.makeText(this,"Por favor llene la verificación de identidad",Toast.LENGTH_SHORT).show();
            i=1;
        }
    }

    private String recibirdatos(int i){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        if(i==1) {
            return extras.getString("id");
        }else{
            return extras.getString("id_mascota");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            Intent intent=new Intent(this,MenuMascota.class);
            intent.putExtra("id",recibirdatos(1));
            intent.putExtra("id_mascota",recibirdatos(2));
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}