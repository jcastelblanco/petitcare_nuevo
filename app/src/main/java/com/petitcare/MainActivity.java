package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1000);
        }
        WorkManagerNotification.setMyContext(getApplicationContext());
    }

    public void iniciarSesion_BUTTON(View view){
        Intent intent = new Intent(this, IniciarSesion.class);
        startActivity(intent);
    }

    public void registrarse_BUTTON(View view){
        Intent i=new Intent(view.getContext(),Registro.class);
        startActivity(i);
    }

    public void recomendacionesIniciales_BUTTON(View view){
        Intent intent=new Intent(this,RecomendacionesParaAdquirirUnaMascota.class);
        startActivity(intent);
    }

}