package com.petitcare;

import com.google.firebase.database.FirebaseDatabase;

public class InitFirebase extends android.app.Application{
    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
