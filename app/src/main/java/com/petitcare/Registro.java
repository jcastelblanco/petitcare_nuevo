package com.petitcare;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Date;

public class Registro extends IniciarSesion {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        inicializarFirebase();
    }

    private DatabaseReference databaseReference;

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    @SuppressLint({"SetTextI18n", "Assert"})
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void crearUsuario_BUTTON(View view){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();

        //Coneccion de formulario con el codigo
       TextView nombresT=findViewById(R.id.nombres);
       TextView apellidosT=findViewById(R.id.apellidos);
       TextView diaT=findViewById(R.id.dia);
       TextView mesT=findViewById(R.id.mes);
       TextView anoT=findViewById(R.id.ano);
       TextView emailT=findViewById(R.id.email);
       TextView confirmaremailT=findViewById(R.id.confirmaremail);
       TextView contrasenaT=findViewById(R.id.contrasena);
       TextView confirmarcontrasenaT=findViewById(R.id.confirmarcontrasena);

       //declaracion de variables necesarias para almacenar y verificar datos del formulario
       boolean registro_completo=false;
       int id_usuario=0;
       String nombres=nombresT.getText().toString();
       String apellidos=apellidosT.getText().toString();
       String dia=diaT.getText().toString();
       String mes=mesT.getText().toString();
       String ano=anoT.getText().toString();
       Date fecha_de_nacimiento=null;
       String email=emailT.getText().toString();
       String confirmaremail=confirmaremailT.getText().toString();
       String contrasena=contrasenaT.getText().toString();
       String confirmarcontrasena=confirmarcontrasenaT.getText().toString();

       //comprobacion para evitar que haya espacios en blanco
       if(!nombres.isEmpty()&&!apellidos.isEmpty()&&!dia.isEmpty()&&!mes.isEmpty()&&!ano.isEmpty()
               &&!email.isEmpty()&&!confirmaremail.isEmpty()&&!contrasena.isEmpty()&&!confirmarcontrasena.isEmpty()){

           //comprobacion de que la fecha sea logica
           if (Integer.parseInt(dia) > 0 && Integer.parseInt(mes) > 0 && Integer.parseInt(ano) > 1910 &&
                   Integer.parseInt(dia) <= 31 && Integer.parseInt(mes) <= 12 && Integer.parseInt(ano)<=3000) {
               fecha_de_nacimiento = new Date(Integer.parseInt(ano) - 1900, Integer.parseInt(mes) - 1, Integer.parseInt(dia));
               registro_completo=true;
           }else{
               Toast.makeText(this,"Fecha invalida",Toast.LENGTH_SHORT).show();
               diaT.setText("");
               mesT.setText("");
               anoT.setText("");
           }

           //funcion de verificar email
           if(!confirmaremail.equals(email)){
               registro_completo=false;
               Toast.makeText(this,"Los e-mail no coinciden",Toast.LENGTH_SHORT).show();
               emailT.setText("");
               confirmaremailT.setText("");
           }

           //funcion de verificar contraseña
           if(!confirmarcontrasena.equals(contrasena)){
               registro_completo=false;
               Toast.makeText(this,"Las contraseñas no coinciden",Toast.LENGTH_SHORT).show();
               contrasenaT.setText("");
               confirmarcontrasenaT.setText("");
           }

           //funcion de verificar que el email sea valido
           if(!email.contains("@")&&!email.contains(".")&&registro_completo){
               Toast.makeText(this,"e-mail invalido",Toast.LENGTH_SHORT).show();
               registro_completo=false;
           }

           //función de verificar largo de contraseña
           if(contrasena.length()<8&&registro_completo){
               Toast.makeText(this,"La contraseña es muy corta, minimo debe contener 8 caracteres",Toast.LENGTH_SHORT).show();
            registro_completo=false;
           }

           System.out.println("Comprobaciones OK");

           }else{
           //alerta si hay espacios vacios
           Toast.makeText(this,"Hay espacios vacios",Toast.LENGTH_SHORT).show();
       }

        //verificacion de que no exista un usuario con el mismo correo ingresado y asignacion de id
        if(registro_completo){
            int i=1;
            while (true){
                //busqueda de usuarios registrados
                String id=Integer.toString(i);
                Cursor cursor = BaseDeDatos.rawQuery
                        ("select email from usuarios where id= " + id, null);
                if(cursor.moveToFirst()){
                    //alerta si se repiten emails
                    if(cursor.getString(0).equals(email)){
                        Toast.makeText(this,"El e-mail ya esta registrado",Toast.LENGTH_SHORT).show();
                        emailT.setText("");
                        confirmaremailT.setText("");
                        registro_completo=false;
                        BaseDeDatos.close();
                        cursor.close();
                        break;
                    }
                    System.out.println("Busqueda OK");
                }else{
                    //asignacion de id para nuevo usuario
                    id_usuario=i;
                    cursor.close();
                    System.out.println("Asignacion OK");
                    break;
                }
                i++;
            }
        }

        //insercion de los datos en la base de datos y revision de estos al final
       if(registro_completo){
            //Ingreso en base de datos
           ContentValues contentValues=new ContentValues();
           contentValues.put("id",Integer.toString(id_usuario));
           System.out.println("id OK");
           contentValues.put("icono",Integer.toString(R.mipmap.aaaa_pecera));
           contentValues.put("nombres",nombres);
           contentValues.put("apellidos",apellidos);
           contentValues.put("fecha_de_nacimiento",fecha_de_nacimiento.toString());
           System.out.println("fecha OK");
           contentValues.put("email",email);
           contentValues.put("contrasena",contrasena);
           System.out.println("email y contraseña OK");
           BaseDeDatos.insert("usuarios",null,contentValues);
           System.out.println("Insercion OK");
           BaseDeDatos.close();

           //verificacion
           System.out.println(id_usuario);
           System.out.println(nombres);
           System.out.println(apellidos);
           System.out.println(fecha_de_nacimiento);
           System.out.println(email);
           System.out.println(contrasena);

           //Creacion de tablas en Firebase
           User usuario = new User(id_usuario+"",email,contrasena,nombres+" "+apellidos);

           databaseReference.child("Usuarios").child(usuario.getId()).setValue(usuario);

           @SuppressLint("RestrictedApi")
           Data data=new Data.Builder().put("titulo","¡Gracias por registrarse!").
                   put("detalle","Esperamos que esta aplicación le sea de utilidad").build();
           WorkManagerNotification.guardarNotificacion(-1,data,"0");
           finish();
       }
    }
}