package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MisMascotas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_mascotas);
        System.out.println(recibirdatos());

        ImageView mascota1=findViewById(R.id.m100);
        ImageView mascota2=findViewById(R.id.m200);
        ImageView mascota3=findViewById(R.id.m300);
        ImageView mascota4=findViewById(R.id.m400);
        ImageView mascota5=findViewById(R.id.m500);
        ImageView mascota6=findViewById(R.id.m600);
        ImageView mascota7=findViewById(R.id.m700);
        ImageView mascota8=findViewById(R.id.m800);
        ImageView mascota9=findViewById(R.id.m900);

        TextView MT1=findViewById(R.id.MT1);
        TextView MT2=findViewById(R.id.MT2);
        TextView MT3=findViewById(R.id.MT3);
        TextView MT4=findViewById(R.id.MT4);
        TextView MT5=findViewById(R.id.MT5);
        TextView MT6=findViewById(R.id.MT6);
        TextView MT7=findViewById(R.id.MT7);
        TextView MT8=findViewById(R.id.MT8);
        TextView MT9=findViewById(R.id.MT9);

        short j= GestorDeMascotaSeleccionada.busquedaDeMascotas(recibirdatos(),mascota1,MT1,mascota2,MT2,mascota3,MT3,
                mascota4,MT4,mascota5,MT5,mascota6,MT6,mascota7,MT7,mascota8,MT8,mascota9,MT9);
        GestorDeMascotaSeleccionada.botonAgregarMascota(j,mascota1,mascota2,mascota3,mascota4,mascota5,mascota6,mascota7,mascota8,mascota9);
    }

    public void verMascota_BUTTON(View view){
        String descripcion=view.getContentDescription().toString();
        if(descripcion.equals("mas")){
            Intent intent=new Intent(this,SeleccionMascota.class);
            intent.putExtra("id",recibirdatos());
            startActivity(intent);
            finish();
        }else if(!descripcion.equals("100")){
            Intent intent=new Intent(this,MenuMascota.class);
            intent.putExtra("id",recibirdatos());
            intent.putExtra("id_mascota",descripcion);
            startActivity(intent);
            finish();
        }
    }

    private String recibirdatos() {
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }

}