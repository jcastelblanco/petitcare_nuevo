package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Social extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
    }

    public void Publicar(View view){
        Intent intent = new Intent(this, Publicacion.class);
        intent.putExtra("id",recibirdatos());
        startActivity(intent);
    }

    public void Foro(View view){
        Intent intent = new Intent(this, Foro.class);
        startActivity(intent);
    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }
}
