package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Rutinas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rutinas);
        System.out.println(recibirdatos(1));
        ImageView tiempo=findViewById(R.id.tiempoIM);
        tiempo.setImageResource(iconoTiempo());
        if(!llenarRutinas()){
            setContentView(R.layout.activity_rutinas_alternativo);
        }
    }

    public void eliminarRutinas_BUTTON(View view){
        DialogFragment dialogFragment=new EmergentDialog();
        dialogFragment.show(getSupportFragmentManager(),"tag_01");
    }

    @SuppressLint("SetTextI18n")
    private boolean llenarRutinas(){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();

        TextView alimentacion=findViewById(R.id.alimentacion);
        TextView tiempo=findViewById(R.id.tiempo);
        TextView veterinaria=findViewById(R.id.veterinaria);
        TextView aseo=findViewById(R.id.aseo);
        TextView tienda=findViewById(R.id.tienda);
        TextView otros=findViewById(R.id.otros);
        int i=1;
        int j=0;
        while (true){
            String it = Integer.toString(i);
            Cursor cursor = BaseDeDatos.rawQuery
                    ("select titulo,texto,id_mascota,fecha_programada,concepto,id_usuario from notificaciones where id =" + it, null);
            if(cursor.moveToFirst()){
                String titulo=cursor.getString(0);
                String texto=cursor.getString(1);
                String id_mascota=cursor.getString(2);
                String fecha_programada=cursor.getString(3);
                String concepto=cursor.getString(4);
                String id_usuario=cursor.getString(5);
                if(id_usuario.equals(recibirdatos(1))&&recibirdatos(2).equals("NO")){
                    switch (concepto){
                        case "otros":
                            String tempText=otros.getText().toString();
                            otros.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "alimentacion":
                            tempText=alimentacion.getText().toString();
                            alimentacion.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "veterinaria":
                        case "Recordatorio Vacunación":
                            tempText=veterinaria.getText().toString();
                            veterinaria.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "tienda":
                            tempText=tienda.getText().toString();
                            tienda.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "aseo":
                            tempText=aseo.getText().toString();
                            aseo.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "tiempo":
                            tempText=tiempo.getText().toString();
                            tiempo.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Codigo para eliminar: "+it+"\n\n");
                            break;
                        default:
                            break;
                    }
                    j++;
                }else if(recibirdatos(2).equals("SI")&&id_mascota.equals(GestorDeMascotaSeleccionada.getId())){
                    switch (concepto){
                        case "otros":
                            String tempText=otros.getText().toString();
                            otros.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "alimentacion":
                            tempText=alimentacion.getText().toString();
                            alimentacion.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "veterinaria":
                        case "Recordatorio Vacunación":
                            tempText=veterinaria.getText().toString();
                            veterinaria.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "tienda":
                            tempText=tienda.getText().toString();
                            tienda.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "aseo":
                            tempText=aseo.getText().toString();
                            aseo.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Código para eliminar: "+it+"\n\n");
                            break;
                        case "tiempo":
                            tempText=tiempo.getText().toString();
                            tiempo.setText(tempText+titulo+" ("+fecha_programada+") \n"+
                                    texto+"\n"+"Codigo para eliminar: "+it+"\n\n");
                            break;
                        default:
                            break;
                    }
                    j++;
                }
                i++;
            }else{
                if(j==0){
                    return false;
                }
                cursor.close();
                BaseDeDatos.close();
                break;
            }
        }
        return true;
    }

    private String recibirdatos(int i){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        if(i==1){
            return extras.getString("id");
        }else {
            return extras.getString("Es una mascota");
        }
    }

    @SuppressLint("SetTextI18n")
    private int iconoTiempo() {
        if(recibirdatos(2).equals("SI")) {
            TextView titulo=findViewById(R.id.tv_historial);
            titulo.setText("Historial de Rutinas y Recordatorios de "+GestorDeMascotaSeleccionada.getNombre());
            switch (GestorDeMascotaSeleccionada.getEspecie()) {
                case "Aves Ornamentales":
                    return R.mipmap.tiempo_aves;
                case "Perros":
                    return R.mipmap.tiempo_perros;
                case "Peces de acuario":
                    return R.mipmap.tiempo_peces;
                case "Roedores":
                    return R.mipmap.tiempo_hamsters;
                case "Gatos":
                    return R.mipmap.tiempo_gatos;
                default:
                    return 0;
            }
        }else {
            return R.mipmap.tiempo;
        }
    }
}