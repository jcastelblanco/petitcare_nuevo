package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Publicacion extends AppCompatActivity {

    private EditText titleText;
    private EditText bodyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicacion);

        titleText = (EditText) findViewById(R.id.titulo_publicacion);
        bodyText = (EditText) findViewById(R.id.cuerpo_publicacion);
    }

    public void CrearPublicacion(View view){
        Intent intent = new Intent(this, Foro.class);
        intent.putExtra("id",recibirdatos());
        intent.putExtra("titulo_texto", titleText.getText().toString());
        intent.putExtra("cuerpo_texto", bodyText.getText().toString());
        startActivity(intent);

    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }
}