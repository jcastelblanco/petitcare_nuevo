package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;
import androidx.work.WorkManager;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Date;
import java.util.Calendar;

public class GestionUsuario extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_usuario);
        System.out.println(recibirdatos());
        datosDeUsuario(recibirdatos());
        TextView titulo=findViewById(R.id.titulo4);
        titulo.setText(nombres+" "+apellidos);
        TextView cumplea_os=findViewById(R.id.cumplea_os);
        cumplea_os.setText(fecha_de_nacimientoU);
        try {
            Calendar calendar1=Calendar.getInstance();
            calendar1.setTime(Date.valueOf(fecha_de_nacimientoU));
            Calendar calendar=Calendar.getInstance();
            if(calendar.get(Calendar.MONTH)==calendar1.get(Calendar.MONTH)
                &&calendar.get(Calendar.DAY_OF_MONTH)==calendar1.get(Calendar.DAY_OF_MONTH)){
                Toast.makeText(this,"\uD83D\uDC36 ¡Feliz cumpleaños "+nombres+"! \uD83D\uDC2D",Toast.LENGTH_LONG).show();
            }
        }catch (Exception ex){
            System.out.println("ERROR EN FECHA");
        }
        i=1;
    }

    private int nuevoIcono;
    private String nombres;
    private String apellidos;
    private String fecha_de_nacimientoU;
    private String emailU;
    private String contrasenaU;

    @SuppressLint("SetTextI18n")
    public void icono_BUTTON(View view){
        String description=view.getContentDescription().toString();
        TextView nuevo_icono=findViewById(R.id.nuevo_icono);
        switch (Integer.parseInt(description)){
            case 1500055:
                nuevo_icono.setText("Pecera");
                nuevoIcono=R.mipmap.aaaa_pecera;
                break;
            case 1500003:
                nuevo_icono.setText("Hombre con su perro");
                nuevoIcono=R.mipmap.aaaa_user_perro;
                break;
            case 1500080:
                nuevo_icono.setText("Mujer con su gato");
                nuevoIcono=R.mipmap.aaaa_user_gato;
                break;
            case 1500034:
                nuevo_icono.setText("Loro");
                nuevoIcono=R.mipmap.aaaa_parrot;
                break;
            case 1500012:
                nuevo_icono.setText("Perro corriendo");
                nuevoIcono=R.mipmap.aaaa_perrito_corriendo;
                break;
            case 1500044:
                nuevo_icono.setText("Gato");
                nuevoIcono=R.mipmap.aaaa_cat_looking;
                break;
            default:
                nuevo_icono.setText("¡OH NO!");
                nuevoIcono=R.mipmap.aaaa_pecera;
                break;
        }
    }

    public void guardarCambios_BUTTON(View view){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();

        TextView nuevo_iconoTV=findViewById(R.id.nuevo_icono);
        TextView emailTV=findViewById(R.id.confirmaremail3);
        TextView contrasenaTV=findViewById(R.id.confirmarcontrasena3);

        String nuevo_icono=nuevo_iconoTV.getText().toString();
        String email=emailTV.getText().toString();
        String contrasena=contrasenaTV.getText().toString();

        if(!nuevo_icono.isEmpty()&&!email.isEmpty()&&!contrasena.isEmpty()){
            if(email.equals(emailU)&&contrasena.equals(contrasenaU)){
                ContentValues contentValues=new ContentValues();
                contentValues.put("icono",Integer.toString(nuevoIcono));
                int cambio_completo=BaseDeDatos.update("usuarios",contentValues,"id="+recibirdatos(),null);
                if(cambio_completo==1){
                    Toast.makeText(this,"Se cambio el icono correctamente",Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(this,Menu.class);
                    intent.putExtra("id",recibirdatos());
                    intent.putExtra("icono",nuevoIcono);
                    startActivity(intent);
                    finish();
                }
            }else {
                Toast.makeText(this,"Verificación de identidad invalida",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this,"Para cambiar el icono debe seleccionar uno nuevo y verificar su identidad",Toast.LENGTH_SHORT).show();
        }
    }

    private int i;
    public void eliminar_usuario_BUTTON(View view) {
        TextView confirmaremailT = findViewById(R.id.confirmaremail3);
        TextView confirmarcontrasenaT = findViewById(R.id.confirmarcontrasena3);
        String confirmaremail=confirmaremailT.getText().toString();
        String confirmarcontrasena=confirmarcontrasenaT.getText().toString();
        if(!confirmaremail.isEmpty()&&!confirmarcontrasena.isEmpty()){
            if(confirmarcontrasena.equals(contrasenaU)&&confirmaremail.equals(emailU)) {
                if (i == 1) {
                    Toast.makeText(this, "Oprima otra vez para eliminar su usuario de la aplicación, esto le impedira volver a ver las mascotas y rutinas asociadas a su usuario", Toast.LENGTH_LONG).show();
                    i++;
                } else if (i == 2) {

                    AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "administracion", null, 1);
                    SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();

                    i=1;
                    int k=-10;
                    while (true) {
                        String id=Integer.toString(i);
                        Cursor cursor = BaseDeDatos.
                                rawQuery("select id_usuario from notificaciones where id=" + id, null);
                        if(cursor.moveToFirst()){
                            String id_usuario=cursor.getString(0);
                            if(id_usuario.equals(recibirdatos())){
                                WorkManager.getInstance(WorkManagerNotification.getMyContext()).cancelAllWorkByTag(id);
                                BaseDeDatos.delete("notificaciones","id="+id,null);
                            }
                        }else if (k > 35) {
                            cursor.close();
                            break;
                        }
                        i++;
                        k++;
                    }

                    int eliminado = BaseDeDatos.delete("usuarios", "id=" +recibirdatos(), null);

                    if (eliminado == 1) {
                        //Se crea un usuario con estos valores porque cada usuario tiene asociadas 3 tablas,
                        // ademas de la propia y SQLite permite abrir maximo 3 tablas simultaneamente
                        //Se eliminan unicamente las notificaciones
                        ContentValues contentValues=new ContentValues();
                        contentValues.put("id",recibirdatos());
                        contentValues.put("icono","EMPTY");
                        contentValues.put("nombres","EMPTY");
                        contentValues.put("apellidos","EMPTY");
                        contentValues.put("fecha_de_nacimiento","EMPTY");
                        contentValues.put("email","EMPTY");
                        contentValues.put("contrasena","EMPTY");
                        BaseDeDatos.insert("usuarios",null,contentValues);
                        @SuppressLint("RestrictedApi")
                        Data data=new Data.Builder().put("titulo","Su usuario fue elimado").
                                put("detalle","Esperamos que vuelva a registrarse pronto.").build();
                        WorkManagerNotification.guardarNotificacion(-1,data,"0");
                        BaseDeDatos.close();
                        finish();
                    }
                }
            }else{
                Toast.makeText(this,"Verificación de identidad invalida",Toast.LENGTH_SHORT).show();
                i=1;
            }
        }else{
            Toast.makeText(this,"Por favor llene la verificación de identidad",Toast.LENGTH_SHORT).show();
            i=1;
        }
    }

    public void recomendacionesIniciales_BUTTON(View view){
        Intent intent=new Intent(this,RecomendacionesParaAdquirirUnaMascota.class);
        startActivity(intent);
    }

    private void datosDeUsuario(String id){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();
        //Busqueda de los datos
        System.out.println(id);
        System.out.println("recibir datos OK");
        Cursor cursor=BaseDeDatos.rawQuery
                ("select nombres, apellidos, fecha_de_nacimiento, email, contrasena from usuarios where id ="+id,null);
        if(cursor.moveToFirst()) {
            nombres=cursor.getString(0);
            apellidos=cursor.getString(1);
            fecha_de_nacimientoU=cursor.getString(2);
            emailU=cursor.getString(3);
            contrasenaU=cursor.getString(4);
            cursor.close();
        }else{
            Toast.makeText(this,"ALGO ANDA MAAAAAAAL",Toast.LENGTH_SHORT).show();
        }
    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }

    private int recibiricono(){
        Bundle extras=getIntent().getExtras();
        assert extras!=null;
        return extras.getInt("icono",R.mipmap.aaaa_pecera);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
            SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();
            Intent intent=new Intent(this,Menu.class);
            intent.putExtra("id",recibirdatos());
            intent.putExtra("icono",recibiricono());
            BaseDeDatos.close();
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}