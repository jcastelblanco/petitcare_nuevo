package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GestionRutinas extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_rutinas);
        System.out.println(recibirdatos());
        TextView toque_un_icono=findViewById(R.id.titulo3);
        toque_un_icono.setText("Toque un icono para programar una rutina para "+GestorDeMascotaSeleccionada.getNombre());
        TextView titulo=findViewById(R.id.titulo_de_la_notificacion);
        titulo.setEnabled(false);
        Spinner spinner = findViewById(R.id.periodo);
        String [] opciones={"Intervalo entre avisos","1 repetición","12 horas","1 día","1 semana","1 mes"};
        ArrayAdapter<String> adapter= new ArrayAdapter<>(this,R.layout.spinner_item_calcular, opciones);
        spinner.setAdapter(adapter);
        ImageButton pasarTiempo=findViewById(R.id.button_pasar_tiempo);
        pasarTiempo.setImageResource(iconoTiempo());
    }
    private final Calendar actual=Calendar.getInstance();
    private Calendar calendar=Calendar.getInstance();
    private String tipo_de_notificacion="tipo";

    public void historialDeNotificaciones_BUTTON(View view){
        Intent intent=new Intent(this,Rutinas.class);
        intent.putExtra("id",recibirdatos());
        intent.putExtra("Es una mascota","SI");
        startActivity(intent);
    }

    @SuppressLint("SetTextI18n")
    public void tituloNotificacion_BUTTON(View view){
        TextView titulo=findViewById(R.id.titulo_de_la_notificacion);
        titulo.setEnabled(false);
        titulo.setHint(R.string.alarma_seleccionada);
        String Titulo=view.getContentDescription().toString();
        tipo_de_notificacion=Titulo;
        if(!Titulo.equals("otros")){
            String texto=Titulo+" "+GestorDeMascotaSeleccionada.getNombre();
            float tama_o= (float) (((8*65)/texto.length()));
            titulo.setTextSize(TypedValue.COMPLEX_UNIT_SP, tama_o);
            titulo.setText(texto);
        }else {
            titulo.setText("");
            titulo.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
            titulo.setHint("Escriba un titulo personalizado");
            titulo.setEnabled(true);
        }
    }

    public void seleccionarFecha_BUTTON(View view){
        int ano = actual.get(Calendar.YEAR);
        int mes = actual.get(Calendar.MONTH);
        int dia = actual.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.YEAR,year);
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd, kk:mm");
                String stringFecha=format.format(calendar.getTime());
                TextView fechaYhora=findViewById(R.id.fecha_y_hora);
                fechaYhora.setText(stringFecha);
            }
        }, ano, mes, dia);
        datePickerDialog.show();
    }

    public  void  seleccionarHora_BUTTON(View view){
        int hora = actual.get(Calendar.HOUR_OF_DAY);
        int minutos = actual.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog=new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                calendar.set(Calendar.MINUTE,minute);
                calendar.set(Calendar.SECOND,0);
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd, kk:mm");
                String stringFecha=format.format(calendar.getTime());
                TextView fechaYhora=findViewById(R.id.fecha_y_hora);
                fechaYhora.setText(stringFecha);
            }
        }, hora, minutos,true);
        timePickerDialog.show();
    }

    public void guardarNotificacion_BUTTON(View view){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();

        TextView tituloTV=findViewById(R.id.titulo_de_la_notificacion);
        TextView textoTV=findViewById(R.id.cuerpo_de_notificacon);
        TextView fechaYhoraTV=findViewById(R.id.fecha_y_hora);
        Spinner periodoTV=findViewById(R.id.periodo);

        String titulo=tituloTV.getText().toString();
        String cuerpo=textoTV.getText().toString();
        String fecha_notificacion=fechaYhoraTV.getText().toString();
        String periodo=periodoTV.getSelectedItem().toString();

        boolean registrocompleto=false;

        if(!titulo.isEmpty()&&!cuerpo.isEmpty()&&!fecha_notificacion.isEmpty()&&!periodo.equals("Intervalo entre avisos")){
            registrocompleto=true;
        }else {
            Toast.makeText(this,"Por favor complete todos los datos",Toast.LENGTH_SHORT).show();
        }

        if(cuerpo.length()>42&&registrocompleto){
            Toast.makeText(this,"El texto de la notificación esta muy largo, maximo 40 caracteres",Toast.LENGTH_LONG).show();
            registrocompleto=false;
        }

        if(registrocompleto){
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            String id;
            long tEspera=calendar.getTimeInMillis()-System.currentTimeMillis();
            int icono=iconoNotificacion();
            String fecha_en_que_se_programo=format.format(actual.getTime());

            int i=1;
            while (true) {
                //busqueda de notificaciones registradas
                String it = Integer.toString(i);
                Cursor cursor = BaseDeDatos.rawQuery("select titulo from notificaciones where id =" + it, null);
                //asignacion de id para nueva notificacion
                if (!cursor.moveToFirst()) {
                    cursor.close();
                    id = it;
                    System.out.println("Asignacion OK");
                    break;
                } else {
                    System.out.println("Busqueda OK");
                    i++;
                }
            }
            ContentValues contentValues=new ContentValues();
            System.out.println(id);
            contentValues.put("id",id);
            contentValues.put("titulo",titulo);
            contentValues.put("texto",cuerpo);
            contentValues.put("id_mascota",GestorDeMascotaSeleccionada.getId());
            contentValues.put("fecha_programada",fecha_en_que_se_programo);
            contentValues.put("concepto",concepto());
            contentValues.put("id_usuario",recibirdatos());
            long v=BaseDeDatos.insert("notificaciones",null,contentValues);
            BaseDeDatos.close();
            if(v!=-1){
                if(periodo.equals("1 repetición")) {
                    WorkManagerNotification.guardarNotificacion(tEspera,
                            guardarData(titulo,cuerpo,Integer.parseInt(id),icono),id);
                }else{
                    WorkManagerNotification.guardarNotificacionPeriodica(tEspera,periodicidad(periodo),
                            guardarData(titulo,cuerpo,Integer.parseInt(id),icono),id);
                }
                Toast.makeText(this, "Rutina guardada", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }

    private int iconoNotificacion(){
        switch (tipo_de_notificacion){
            case "otros":
                return R.mipmap.otros;
            case "Recuerde alimentar a:":
                return R.mipmap.comida;
            case "Recuerde llevar a la veterinaria a:":
                return R.mipmap.veterinaria;
            case "Recuerde ir a la tienda por cosas para:":
                return R.mipmap.tienda;
            case "Recuerde asear el hábitat o darle un baño a:":
                return R.mipmap.aseo_mascotas;
            case "Recuerde pasar tiempo con:":
                return iconoTiempo();
            default:
                return R.mipmap.heart_paw_red;
        }
    }

    private String concepto(){
        switch (tipo_de_notificacion){
            case "otros":
                return "otros";
            case "Recuerde alimentar a:":
                return "alimentacion";
            case "Recuerde llevar a la veterinaria a:":
                return "veterinaria";
            case "Recuerde ir a la tienda por cosas para:":
                return "tienda";
            case "Recuerde asear el hábitat o darle un baño a:":
                return "aseo";
            case "Recuerde pasar tiempo con:":
                return "tiempo";
            default:
                return "0";
        }
    }

    private int iconoTiempo(){
        switch (GestorDeMascotaSeleccionada.getEspecie()){
            case "Aves Ornamentales":
                return R.mipmap.tiempo_aves;
            case "Perros":
                return R.mipmap.tiempo_perros;
            case "Peces de acuario":
                return R.mipmap.tiempo_peces;
            case "Roedores":
                return R.mipmap.tiempo_hamsters;
            case "Gatos":
                return R.mipmap.tiempo_gatos;
            default:
                return 0;
        }
    }

    private static int periodicidad(String periodicidad){
        switch (periodicidad){
            case "12 horas":
                return 1;
            case "1 día":
                return 2;
            case "1 semana":
                return 3;
            case "1 mes":
                return 4;
            default:
                return 0;
        }
    }

    private Data guardarData(String titulo, String detalle, int IDnotificacion,int icon){
        return new Data.Builder()
                .putString("titulo",titulo)
                .putString("detalle",detalle)
                .putInt("id",IDnotificacion)
                .putInt("icon",icon).build();
    }
}