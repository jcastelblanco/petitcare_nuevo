package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import android.widget.TextView;
import android.widget.Toast;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Vacunas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(GestorDeMascotaSeleccionada.getEspecie().equals("Perros")
                ||GestorDeMascotaSeleccionada.getEspecie().equals("Gatos")
                ||GestorDeMascotaSeleccionada.getRasgo_distintivo().equals("Loro")
                ||GestorDeMascotaSeleccionada.getRasgo_distintivo().equals("Canario")){
            setContentView(R.layout.activity_vacunas);
            confirmacion=0;
            llenarVacunas();
        }else{
            setContentView(R.layout.activity_vacunas_variante);
        }
        System.out.println(recibirdatos());
    }

    private int confirmacion;
    public void aceptar_BUTTON(View view){

        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();

        TextView modificar_vacuna=findViewById(R.id.modificar_vacuna);
        TextView modificar_fecha=findViewById(R.id.modificar_fecha);
        TextView modificar_veterinaria=findViewById(R.id.modificar_veterinaria);
        TextView modificar_inyectadopor=findViewById(R.id.modificar_inyectadopor);
        Spinner vacunas=findViewById(R.id.spinner_vacunas);

        String opcion_seleccionada=vacunas.getSelectedItem().toString();
        String vacuna=modificar_vacuna.getText().toString();
        String fecha=modificar_fecha.getText().toString();
        String veterinaria=modificar_veterinaria.getText().toString();
        String inyectadopor=modificar_inyectadopor.getText().toString();

        Date fecha_vacuna = null;
        boolean registro_completo=false;
        String id;

        if(fecha.isEmpty()||vacuna.isEmpty()||veterinaria.isEmpty()){
            System.out.println("Hola");
            Toast.makeText(this,"Por favor complete todos los espacios, si va a crear un recordatorio no es necesario llenar el espacio 'Inyectado por'",Toast.LENGTH_LONG).show();
            confirmacion=0;
        }else{
            registro_completo=true;
        }

        try{
            if(registro_completo) {
                fecha_vacuna=Date.valueOf(fecha);
                System.out.println(fecha_vacuna);
            }
        }catch (Exception ex){
            Toast.makeText(this,"La fecha esta mal escrita",Toast.LENGTH_SHORT).show();
            registro_completo=false;
            confirmacion=0;
        }

        if(registro_completo&&opcion_seleccionada.equals("Seleccione una opción")){
            Toast.makeText(this,"Por favor seleccione una opción",Toast.LENGTH_SHORT).show();
            registro_completo=false;
            confirmacion=0;
        }

        if(registro_completo&&confirmacion==1){
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(fecha_vacuna);
            switch (opcion_seleccionada){
                case "Registrar la vacuna":
                    if(inyectadopor.isEmpty()){
                        Toast.makeText(this,"Para registrar la vacuna se requiren todos los datos",Toast.LENGTH_SHORT).show();
                    }else {
                        int i = 1;
                        while (true) {
                            //busqueda de vacunas registradas
                            String it = Integer.toString(i);
                            Cursor cursor = BaseDeDatos.rawQuery("select nombre, id_mascota from carnet_de_vacunacion where id =" + it, null);
                            //asignacion de id para nueva vacuna
                            if (!cursor.moveToFirst()) {
                                cursor.close();
                                id = it;
                                System.out.println("Asignacion OK");
                                break;
                            } else {
                                System.out.println("Busqueda OK");
                                i++;
                            }
                        }
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("id", id);
                        contentValues.put("nombre", vacuna);
                        contentValues.put("fecha", fecha_vacuna.toString());
                        contentValues.put("veterinaria", veterinaria);
                        contentValues.put("inyectadopor", inyectadopor);
                        contentValues.put("id_mascota", GestorDeMascotaSeleccionada.getId());
                        long v=BaseDeDatos.insert("carnet_de_vacunacion",null,contentValues);
                        if(v!=-1){
                            Toast.makeText(this,"Registro completado",Toast.LENGTH_SHORT).show();
                            BaseDeDatos.close();
                            Intent intent=new Intent(this,Vacunas.class);
                            intent.putExtra("id",recibirdatos());
                            startActivity(intent);
                            finish();
                        }
                    }
                    break;
                case "Crear recordatorio(6am)":
                    calendar.set(Calendar.HOUR_OF_DAY,6);
                    calendar.set(Calendar.MINUTE,0);
                    calendar.set(Calendar.SECOND,0);
                    long alertTime=calendar.getTimeInMillis()-System.currentTimeMillis();
                    int i = 1;
                    while (true) {
                        //busqueda de vacunas registradas
                        String it = Integer.toString(i);
                        Cursor cursor = BaseDeDatos.rawQuery("select titulo from notificaciones where id =" + it, null);
                        //asignacion de id para nueva vacuna
                        if (!cursor.moveToFirst()) {
                            cursor.close();
                            id = it;
                            System.out.println("Asignacion OK");
                            break;
                        } else {
                            System.out.println("Busqueda OK");
                            i++;
                        }
                    }
                    ContentValues contentValues=new ContentValues();
                    contentValues.put("id",id);
                    String titulo= "Recordar la vacuna de "+vacuna+" para "+GestorDeMascotaSeleccionada.getNombre();
                    String texto="La vacuna sera aplicada en la veterinaria: "+veterinaria;
                    contentValues.put("titulo",titulo);
                    contentValues.put("texto",texto);
                    contentValues.put("id_mascota",GestorDeMascotaSeleccionada.getId());
                    Calendar calendar1=Calendar.getInstance();
                    String fecha_actual=format.format(calendar1.getTime());
                    contentValues.put("fecha_programada",fecha_actual);
                    contentValues.put("concepto","Recordatorio Vacunación");
                    contentValues.put("id_usuario",recibirdatos());
                    long v=BaseDeDatos.insert("notificaciones",null,contentValues);
                    if(v!=-1){
                        WorkManagerNotification.guardarNotificacion(alertTime, guardarData(titulo,texto,Integer.parseInt(id)), id);
                        Toast.makeText(this, "Recordatorio guardado", Toast.LENGTH_SHORT).show();
                        confirmacion=0;
                    }
                    break;
                case "Crear recordatorio(9am)":
                    calendar.set(Calendar.HOUR_OF_DAY,9);
                    calendar.set(Calendar.MINUTE,0);
                    calendar.set(Calendar.SECOND,0);
                    alertTime=calendar.getTimeInMillis()-System.currentTimeMillis();
                    i = 1;
                    while (true) {
                        //busqueda de vacunas registradas
                        String it = Integer.toString(i);
                        Cursor cursor = BaseDeDatos.rawQuery("select titulo from notificaciones where id =" + it, null);
                        //asignacion de id para nueva vacuna
                        if (!cursor.moveToFirst()) {
                            cursor.close();
                            id = it;
                            System.out.println("Asignacion OK");
                            break;
                        } else {
                            System.out.println("Busqueda OK");
                            i++;
                        }
                    }
                    contentValues=new ContentValues();
                    contentValues.clear();
                    contentValues.put("id",id);
                    titulo= "Recordar la vacuna de "+vacuna+" para "+GestorDeMascotaSeleccionada.getNombre();
                    texto="La vacuna sera aplicada en la veterinaria: "+veterinaria;
                    contentValues.put("titulo",titulo);
                    contentValues.put("texto",texto);
                    contentValues.put("id_mascota",GestorDeMascotaSeleccionada.getId());
                    calendar1=Calendar.getInstance();
                    fecha_actual=format.format(calendar1.getTime());
                    contentValues.put("fecha_programada",fecha_actual);
                    contentValues.put("concepto","Recordatorio Vacunación");
                    contentValues.put("id_usuario",recibirdatos());
                    v=BaseDeDatos.insert("notificaciones",null,contentValues);
                    if(v!=-1){
                        WorkManagerNotification.guardarNotificacion(alertTime, guardarData(titulo,texto,Integer.parseInt(id)), id);
                        Toast.makeText(this, "Recordatorio guardado", Toast.LENGTH_SHORT).show();
                        confirmacion=0;
                    }
                    break;
                case "Crear recordatorio(12pm)":
                    calendar.set(Calendar.HOUR_OF_DAY,12);
                    calendar.set(Calendar.MINUTE,0);
                    calendar.set(Calendar.SECOND,0);
                    alertTime=calendar.getTimeInMillis()-System.currentTimeMillis();
                    i = 1;
                    while (true) {
                        //busqueda de vacunas registradas
                        String it = Integer.toString(i);
                        Cursor cursor = BaseDeDatos.rawQuery("select titulo from notificaciones where id =" + it, null);
                        //asignacion de id para nueva vacuna
                        if (!cursor.moveToFirst()) {
                            cursor.close();
                            id = it;
                            System.out.println("Asignacion OK");
                            break;
                        } else {
                            System.out.println("Busqueda OK");
                            i++;
                        }
                    }
                    contentValues=new ContentValues();
                    contentValues.clear();
                    contentValues.put("id",id);
                    titulo= "Recordar la vacuna de "+vacuna+" para "+GestorDeMascotaSeleccionada.getNombre();
                    texto="La vacuna sera aplicada en la veterinaria: "+veterinaria;
                    contentValues.put("titulo",titulo);
                    contentValues.put("texto",texto);
                    contentValues.put("id_mascota",GestorDeMascotaSeleccionada.getId());
                    calendar1=Calendar.getInstance();
                    fecha_actual=format.format(calendar1.getTime());
                    contentValues.put("fecha_programada",fecha_actual);
                    contentValues.put("concepto","Recordatorio Vacunación");
                    contentValues.put("id_usuario",recibirdatos());
                    v=BaseDeDatos.insert("notificaciones",null,contentValues);
                    if(v!=-1){
                        WorkManagerNotification.guardarNotificacion(alertTime, guardarData(titulo,texto,Integer.parseInt(id)), id);
                        Toast.makeText(this, "Recordatorio guardado", Toast.LENGTH_SHORT).show();
                        confirmacion=0;
                    }
                    break;
                case "Crear recordatorio(3pm)":
                    calendar.set(Calendar.HOUR_OF_DAY,15);
                    calendar.set(Calendar.MINUTE,0);
                    calendar.set(Calendar.SECOND,0);
                    alertTime=calendar.getTimeInMillis()-System.currentTimeMillis();
                    i = 1;
                    while (true) {
                        //busqueda de vacunas registradas
                        String it = Integer.toString(i);
                        Cursor cursor = BaseDeDatos.rawQuery("select titulo from notificaciones where id =" + it, null);
                        //asignacion de id para nueva vacuna
                        if (!cursor.moveToFirst()) {
                            cursor.close();
                            id = it;
                            System.out.println("Asignacion OK");
                            break;
                        } else {
                            System.out.println("Busqueda OK");
                            i++;
                        }
                    }
                    contentValues=new ContentValues();
                    contentValues.clear();
                    contentValues.put("id",id);
                    titulo= "Recordar la vacuna de "+vacuna+" para "+GestorDeMascotaSeleccionada.getNombre();
                    texto="La vacuna sera aplicada en la veterinaria: "+veterinaria;
                    contentValues.put("titulo",titulo);
                    contentValues.put("texto",texto);
                    contentValues.put("id_mascota",GestorDeMascotaSeleccionada.getId());
                    calendar1=Calendar.getInstance();
                    fecha_actual=format.format(calendar1.getTime());
                    contentValues.put("fecha_programada",fecha_actual);
                    contentValues.put("concepto","Recordatorio Vacunación");
                    contentValues.put("id_usuario",recibirdatos());
                    v=BaseDeDatos.insert("notificaciones",null,contentValues);
                    if(v!=-1){
                        WorkManagerNotification.guardarNotificacion(alertTime, guardarData(titulo,texto,Integer.parseInt(id)), id);
                        Toast.makeText(this, "Recordatorio guardado", Toast.LENGTH_SHORT).show();
                        confirmacion=0;
                    }
                    break;
                case "Crear recordatorio(6pm)":
                    calendar.set(Calendar.HOUR_OF_DAY,18);
                    calendar.set(Calendar.MINUTE,0);
                    calendar.set(Calendar.SECOND,0);
                    alertTime=calendar.getTimeInMillis()-System.currentTimeMillis();
                    i = 1;
                    while (true) {
                        //busqueda de vacunas registradas
                        String it = Integer.toString(i);
                        Cursor cursor = BaseDeDatos.rawQuery("select titulo from notificaciones where id =" + it, null);
                        //asignacion de id para nueva vacuna
                        if (!cursor.moveToFirst()) {
                            cursor.close();
                            id = it;
                            System.out.println("Asignacion OK");
                            break;
                        } else {
                            System.out.println("Busqueda OK");
                            i++;
                        }
                    }
                    contentValues=new ContentValues();
                    contentValues.clear();
                    contentValues.put("id",id);
                    titulo= "Recordar la vacuna de "+vacuna+" para "+GestorDeMascotaSeleccionada.getNombre();
                    texto="La vacuna sera aplicada en la veterinaria: "+veterinaria;
                    contentValues.put("titulo",titulo);
                    contentValues.put("texto",texto);
                    contentValues.put("id_mascota",GestorDeMascotaSeleccionada.getId());
                    calendar1=Calendar.getInstance();
                    fecha_actual=format.format(calendar1.getTime());
                    contentValues.put("fecha_programada",fecha_actual);
                    contentValues.put("concepto","Recordatorio Vacunación");
                    contentValues.put("id_usuario",recibirdatos());
                    v=BaseDeDatos.insert("notificaciones",null,contentValues);
                    if(v!=-1){
                        WorkManagerNotification.guardarNotificacion(alertTime, guardarData(titulo,texto,Integer.parseInt(id)),id);
                        Toast.makeText(this, "Recordatorio guardado", Toast.LENGTH_SHORT).show();
                        confirmacion=0;
                    }
                    break;
                default:
                    break;
            }
        }else if(registro_completo){
            Toast.makeText(this,"Confirme que los valores sean correctos y oprima nuevamente",Toast.LENGTH_SHORT).show();
            confirmacion++;
        }
    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }

    @SuppressLint("SetTextI18n")
    private void llenarVacunas(){
        AdminSQLiteOpenHelper admin=new AdminSQLiteOpenHelper(this, "administracion",null, 1);
        SQLiteDatabase BaseDeDatos=admin.getWritableDatabase();

        Spinner vacunas=findViewById(R.id.spinner_vacunas);
        String [] opciones={"Seleccione una opción","Registrar la vacuna","Crear recordatorio(6am)","Crear recordatorio(9am)",
                "Crear recordatorio(12pm)","Crear recordatorio(3pm)","Crear recordatorio(6pm)"};
        ArrayAdapter<String> adapter= new ArrayAdapter<>(this,R.layout.spinner_item_calcular, opciones);
        vacunas.setAdapter(adapter);

        TextView titulo=findViewById(R.id.titulo);
        titulo.setText("Carnet de vacunación de "+GestorDeMascotaSeleccionada.getNombre());

        TextView[] datos_vacuna=camposVacunas();
        int it=1;
        int mult=0;
        while(true) {
            String id=Integer.toString(it);
            Cursor cursor = BaseDeDatos.rawQuery
                    ("select nombre, fecha, veterinaria, inyectadopor, id_mascota from carnet_de_vacunacion where id="+id, null);
            if(cursor.moveToFirst()){
                String nombre=cursor.getString(0);
                String fecha=cursor.getString(1);
                String veterinaria=cursor.getString(2);
                String inyectadopor=cursor.getString(3);
                String id_mascota=cursor.getString(4);
                if(id_mascota.equals(GestorDeMascotaSeleccionada.getId())){
                    mult++;
                    if(mult>21){ cursor.close();BaseDeDatos.close();break; }
                    datos_vacuna[(mult*4)-4].setText(nombre);
                    datos_vacuna[(mult*4)-3].setText(fecha);
                    datos_vacuna[(mult*4)-2].setText(veterinaria);
                    datos_vacuna[(mult*4)-1].setText(inyectadopor);
                }
                it++;
            }else{
                cursor.close();
                BaseDeDatos.close();
                break;
            }
        }
    }

    private Data guardarData(String titulo, String detalle, int IDnotificacion){
        return new Data.Builder().putString("titulo",titulo)
                .putString("detalle",detalle)
                .putInt("id",IDnotificacion)
                .putInt("icon",R.mipmap.vacuna).build();
    }

    private TextView[] camposVacunas(){
        TextView[] campos = new TextView[84];

        campos[0]=findViewById(R.id.v1v);
        campos[1]=findViewById(R.id.v1f);
        campos[2]=findViewById(R.id.v1t);
        campos[3]=findViewById(R.id.v1i);

        campos[4]=findViewById(R.id.v2v);
        campos[5]=findViewById(R.id.v2f);
        campos[6]=findViewById(R.id.v2t);
        campos[7]=findViewById(R.id.v2i);

        campos[8]=findViewById(R.id.v3v);
        campos[9]=findViewById(R.id.v3f);
        campos[10]=findViewById(R.id.v3t);
        campos[11]=findViewById(R.id.v3i);

        campos[12]=findViewById(R.id.v4v);
        campos[13]=findViewById(R.id.v4f);
        campos[14]=findViewById(R.id.v4t);
        campos[15]=findViewById(R.id.v4i);

        campos[16]=findViewById(R.id.v5v);
        campos[17]=findViewById(R.id.v5f);
        campos[18]=findViewById(R.id.v5t);
        campos[19]=findViewById(R.id.v5i);

        campos[20]=findViewById(R.id.v6v);
        campos[21]=findViewById(R.id.v6f);
        campos[22]=findViewById(R.id.v6t);
        campos[23]=findViewById(R.id.v6i);

        campos[24]=findViewById(R.id.v7v);
        campos[25]=findViewById(R.id.v7f);
        campos[26]=findViewById(R.id.v7t);
        campos[27]=findViewById(R.id.v7i);

        campos[28]=findViewById(R.id.v8v);
        campos[29]=findViewById(R.id.v8f);
        campos[30]=findViewById(R.id.v8t);
        campos[31]=findViewById(R.id.v8i);

        campos[32]=findViewById(R.id.v9v);
        campos[33]=findViewById(R.id.v9f);
        campos[34]=findViewById(R.id.v9t);
        campos[35]=findViewById(R.id.v9i);

        campos[36]=findViewById(R.id.v10v);
        campos[37]=findViewById(R.id.v10f);
        campos[38]=findViewById(R.id.v10t);
        campos[39]=findViewById(R.id.v10i);

        campos[40]=findViewById(R.id.v11v);
        campos[41]=findViewById(R.id.v11f);
        campos[42]=findViewById(R.id.v11t);
        campos[43]=findViewById(R.id.v11i);

        campos[44]=findViewById(R.id.v12v);
        campos[45]=findViewById(R.id.v12f);
        campos[46]=findViewById(R.id.v12t);
        campos[47]=findViewById(R.id.v12i);

        campos[48]=findViewById(R.id.v13v);
        campos[49]=findViewById(R.id.v13f);
        campos[50]=findViewById(R.id.v13t);
        campos[51]=findViewById(R.id.v13i);

        campos[52]=findViewById(R.id.v14v);
        campos[53]=findViewById(R.id.v14f);
        campos[54]=findViewById(R.id.v14t);
        campos[55]=findViewById(R.id.v14i);

        campos[56]=findViewById(R.id.v15v);
        campos[57]=findViewById(R.id.v15f);
        campos[58]=findViewById(R.id.v15t);
        campos[59]=findViewById(R.id.v15i);

        campos[60]=findViewById(R.id.v16v);
        campos[61]=findViewById(R.id.v16f);
        campos[62]=findViewById(R.id.v16t);
        campos[63]=findViewById(R.id.v16i);

        campos[64]=findViewById(R.id.v17v);
        campos[65]=findViewById(R.id.v17f);
        campos[66]=findViewById(R.id.v17t);
        campos[67]=findViewById(R.id.v17i);

        campos[68]=findViewById(R.id.v18v);
        campos[69]=findViewById(R.id.v18f);
        campos[70]=findViewById(R.id.v18t);
        campos[71]=findViewById(R.id.v18i);

        campos[72]=findViewById(R.id.v19v);
        campos[73]=findViewById(R.id.v19f);
        campos[74]=findViewById(R.id.v19t);
        campos[75]=findViewById(R.id.v19i);

        campos[76]=findViewById(R.id.v20v);
        campos[77]=findViewById(R.id.v20f);
        campos[78]=findViewById(R.id.v20t);
        campos[79]=findViewById(R.id.v20i);

        campos[80]=findViewById(R.id.v21v);
        campos[81]=findViewById(R.id.v21f);
        campos[82]=findViewById(R.id.v21t);
        campos[83]=findViewById(R.id.v21i);
        return campos;
    }
}