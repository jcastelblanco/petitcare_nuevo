package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class RecomendacionesParaAdquirirUnaMascota extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomendaciones_para_adquirir_una_mascota);
    }

    public void recomendacionesSegunMascota_BUTTON(View view){
        String descripcion=view.getContentDescription().toString();
        Intent intent=new Intent(this,RecomendacionesSegunMascota.class);
        intent.putExtra("descripcion",descripcion);
        System.out.println(descripcion);
        startActivity(intent);
    }
}