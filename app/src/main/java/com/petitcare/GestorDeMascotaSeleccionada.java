package com.petitcare;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

class GestorDeMascotaSeleccionada {

    private static String id;
    private static String especie;
    private static String nombre;
    private static int edad;
    private static String rasgo_distintivo;

    public static String getId() {
        return id;
    }

    public static void setid(String id) {
        GestorDeMascotaSeleccionada.id = id;
    }

    public static String getEspecie() {
        return especie;
    }

    public static void setEspecie(String especie) {
        GestorDeMascotaSeleccionada.especie = especie;
    }

    public static String getNombre() {
        return nombre;
    }

    public static void setNombre(String nombre) {
        GestorDeMascotaSeleccionada.nombre = nombre;
    }

    public static int getEdad() {
        return edad;
    }

    public static void setEdad(String edad) {
        GestorDeMascotaSeleccionada.edad = Integer.parseInt(edad);
    }

    public static String getRasgo_distintivo() {
        return rasgo_distintivo;
    }

    public static void setRasgo_distintivo(String rasgo_distintivo) {
        GestorDeMascotaSeleccionada.rasgo_distintivo = rasgo_distintivo;
    }

    protected static short busquedaDeMascotas(String id_usuario_actual,ImageView mascota1,TextView MT1
            ,ImageView mascota2,TextView MT2,ImageView mascota3,TextView MT3,ImageView mascota4,TextView MT4
            ,ImageView mascota5,TextView MT5,ImageView mascota6,TextView MT6,ImageView mascota7,TextView MT7
            ,ImageView mascota8,TextView MT8,ImageView mascota9,TextView MT9){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(mascota1.getContext(), "administracion", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();
        short i=1;
        short j=100;
        short k=0;
        while (true) {
            String idmas=Short.toString(i);
            Cursor cursor = BaseDeDatos.
                    rawQuery("select especie, nombre, id_dueno from mascotas where id=" + idmas, null);
            if(cursor.moveToFirst()){
                String id_dueno=cursor.getString(2);
                String nombre=cursor.getString(1);
                String especie=cursor.getString(0);
                cursor.close();
                if(id_dueno.equals(id_usuario_actual)){
                    switch (j){
                        case 100:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota1,MT1,nombre,especie);
                            mascota1.setContentDescription(idmas);
                            break;
                        case 200:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota2,MT2,nombre,especie);
                            mascota2.setContentDescription(idmas);
                            break;
                        case 300:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota3,MT3,nombre,especie);
                            mascota3.setContentDescription(idmas);
                            break;
                        case 400:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota4,MT4,nombre,especie);
                            mascota4.setContentDescription(idmas);
                            break;
                        case 500:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota5,MT5,nombre,especie);
                            mascota5.setContentDescription(idmas);
                            break;
                        case 600:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota6,MT6,nombre,especie);
                            mascota6.setContentDescription(idmas);
                            break;
                        case 700:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota7,MT7,nombre,especie);
                            mascota7.setContentDescription(idmas);
                            break;
                        case 800:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota8,MT8,nombre,especie);
                            mascota8.setContentDescription(idmas);
                            break;
                        case 900:
                            GestorDeMascotaSeleccionada.imagenYNombre(mascota9,MT9,nombre,especie);
                            mascota9.setContentDescription(idmas);
                            break;
                        default:
                            break;
                    }
                    j+=100;
                    k=0;
                }
            }else if(k>35){
                cursor.close();
                BaseDeDatos.close();
                break;
            }
            i++;
            k++;
        }
        return j;
    }

    private static void imagenYNombre(ImageView imageView, TextView textView, String nombre, String especie){
        switch (especie){
            case "Aves Ornamentales":
                imageView.setImageResource(R.mipmap.bird512);
                break;
            case "Perros":
                imageView.setImageResource(R.mipmap.dog512);
                break;
            case "Peces de acuario":
                imageView.setImageResource(R.mipmap.fish512);
                break;
            case "Roedores":
                imageView.setImageResource(R.mipmap.hamster512);
                break;
            case "Gatos":
                imageView.setImageResource(R.mipmap.cat512);
                break;
            default:
                break;
        }
        textView.setText(nombre);
    }

    protected static void botonAgregarMascota(int j,ImageView mascota1,ImageView mascota2,ImageView mascota3
            ,ImageView mascota4,ImageView mascota5,ImageView mascota6,ImageView mascota7,ImageView mascota8,ImageView mascota9){
        switch (j){
            case 100:
                mascota1.setImageResource(R.mipmap.signo_de_mas);
                mascota1.setContentDescription("mas");
                mascota2.setVisibility(View.INVISIBLE);
                mascota3.setVisibility(View.INVISIBLE);
                mascota4.setVisibility(View.INVISIBLE);
                mascota5.setVisibility(View.INVISIBLE);
                mascota6.setVisibility(View.INVISIBLE);
                mascota7.setVisibility(View.INVISIBLE);
                mascota8.setVisibility(View.INVISIBLE);
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 200:
                mascota2.setImageResource(R.mipmap.signo_de_mas);
                mascota2.setContentDescription("mas");
                mascota3.setVisibility(View.INVISIBLE);
                mascota4.setVisibility(View.INVISIBLE);
                mascota5.setVisibility(View.INVISIBLE);
                mascota6.setVisibility(View.INVISIBLE);
                mascota7.setVisibility(View.INVISIBLE);
                mascota8.setVisibility(View.INVISIBLE);
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 300:
                mascota3.setImageResource(R.mipmap.signo_de_mas);
                mascota3.setContentDescription("mas");
                mascota4.setVisibility(View.INVISIBLE);
                mascota5.setVisibility(View.INVISIBLE);
                mascota6.setVisibility(View.INVISIBLE);
                mascota7.setVisibility(View.INVISIBLE);
                mascota8.setVisibility(View.INVISIBLE);
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 400:
                mascota4.setImageResource(R.mipmap.signo_de_mas);
                mascota4.setContentDescription("mas");
                mascota5.setVisibility(View.INVISIBLE);
                mascota6.setVisibility(View.INVISIBLE);
                mascota7.setVisibility(View.INVISIBLE);
                mascota8.setVisibility(View.INVISIBLE);
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 500:
                mascota5.setImageResource(R.mipmap.signo_de_mas);
                mascota5.setContentDescription("mas");
                mascota6.setVisibility(View.INVISIBLE);
                mascota7.setVisibility(View.INVISIBLE);
                mascota8.setVisibility(View.INVISIBLE);
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 600:
                mascota6.setImageResource(R.mipmap.signo_de_mas);
                mascota6.setContentDescription("mas");
                mascota7.setVisibility(View.INVISIBLE);
                mascota8.setVisibility(View.INVISIBLE);
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 700:
                mascota7.setImageResource(R.mipmap.signo_de_mas);
                mascota7.setContentDescription("mas");
                mascota8.setVisibility(View.INVISIBLE);
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 800:
                mascota8.setImageResource(R.mipmap.signo_de_mas);
                mascota8.setContentDescription("mas");
                mascota9.setVisibility(View.INVISIBLE);
                break;
            case 900:
                mascota9.setImageResource(R.mipmap.signo_de_mas);
                mascota9.setContentDescription("mas");
                break;
            default:
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    protected static void menuMascotas(String id_mascota, TextView especieT, TextView nombreT,
                                       TextView edadT, TextView rasgo_distintivoT,ImageView foto_mascota){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(foto_mascota.getContext(), "administracion", null, 1);
        SQLiteDatabase BaseDeDatos = admin.getWritableDatabase();

        Cursor cursor=BaseDeDatos.rawQuery
                ("select especie, nombre, edad, rasgo_distintivo, imagepath from mascotas where id="+id_mascota,null);
        if(cursor.moveToFirst()){
            String especie=cursor.getString(0);
            if(especie.equals("Aves Ornamentales")||especie.equals("Peces de acuario")||especie.equals("Roedores")){

                especieT.setText(R.string.especie);
            }
            String nombre=cursor.getString(1);

            nombreT.setText(nombre);
            String edad=cursor.getString(2);
            if(Integer.parseInt(edad)>1) {

                edadT.setText(edad+" años cumplidos");
            }else{
                edadT.setText(edad+" año cumplido");
            }
            String rasgo_distintivo=cursor.getString(3);

            rasgo_distintivoT.setText(rasgo_distintivo);
            String imagepath=cursor.getString(4);
            foto_mascota.setImageURI(Uri.parse(imagepath));
            cursor.close();

            GestorDeMascotaSeleccionada.setid(id_mascota);
            GestorDeMascotaSeleccionada.setEspecie(especie);
            GestorDeMascotaSeleccionada.setNombre(nombre);
            GestorDeMascotaSeleccionada.setEdad(edad);
            GestorDeMascotaSeleccionada.setRasgo_distintivo(rasgo_distintivo);

        }else{
            cursor.close();
            Toast.makeText(foto_mascota.getContext(),"Ha ocurrido un error",Toast.LENGTH_SHORT).show();
        }
    }
}