package com.petitcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RutinasYVacunas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rutinas_y_vacunas);
        System.out.println(recibirdatos());

        TextView nombre_mascota=findViewById(R.id.nombre_de_mascota);
        nombre_mascota.setText(GestorDeMascotaSeleccionada.getNombre());
        i=1;
    }

    private int i;
    public void vacunas_BUTTON(View view){
        Intent intent = new Intent(this, Vacunas.class);
        if(i==1) {
            Toast.makeText(this, "Por favor gire el telefono y oprima nuevamente", Toast.LENGTH_SHORT).show();
            i++;
        }else if(i==2){
            intent.putExtra("id", recibirdatos());
            startActivity(intent);
        }
    }

    public void gestionRutinas_BUTTON(View view){
        Intent intent=new Intent(this,GestionRutinas.class);
        intent.putExtra("id",recibirdatos());
        startActivity(intent);
    }

    public void importancioDeVacunar_BUTTON(View view){
        Intent intent=new Intent(this,ImportanciaDeVacunarLasMascotas.class);
        intent.putExtra("id",recibirdatos());
        startActivity(intent);
    }

    private String recibirdatos(){
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        return extras.getString("id");
    }
}